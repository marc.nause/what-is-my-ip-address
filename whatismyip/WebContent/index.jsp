<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="de.audioattack.whatismyip.Tools"%>

<% final Tools tools = new Tools(request); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>What is my IP address?</title>
</head>
<body>
	<h1>What is my IP address?</h1>
	<p>Your IP address: <%= tools.getRemoteAddr() %> (<%= tools.getRemoteHostName() %>)</p>
	<p>Transparent Proxy: <%= tools.isProxyUsed() %> <%= tools.getForwardedForIPs() %></p>
</body>
</html>