package de.audioattack.whatismyip;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

public class Tools {

    public final static String X_FORWARDED_FOR = "x-forwarded-for";

    private final String remoteAddr;
    
    private final String remoteHostName;

    private final boolean proxy;
    
    private String forwarded = "";
    
    private final static int NUM_OF_REVERSE_PROXIES = 1;

    public Tools(final HttpServletRequest request) {
        final String remoteAddr = request.getRemoteAddr();

        final String forwardedFor = request.getHeader(X_FORWARDED_FOR);

        InetAddress inetAddr = null;
        try {
            inetAddr = InetAddress.getByName(remoteAddr);
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (inetAddr == null ||
                inetAddr.isLoopbackAddress() && forwardedFor != null) {
            final String[] addrs = forwardedFor.split(",");
            this.remoteAddr = addrs[addrs.length - 1].trim();
            try {
                inetAddr = InetAddress.getByName(this.remoteAddr);
            } catch (UnknownHostException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            proxy = addrs.length > NUM_OF_REVERSE_PROXIES;
            forwarded = Arrays.deepToString(
                    Arrays.copyOfRange(
                            addrs,
                            0,
                            Math.max(0, addrs.length - NUM_OF_REVERSE_PROXIES)));
        } else {
            proxy = false;
            this.remoteAddr = remoteAddr;
        }
        
        if (inetAddr != null) {
            remoteHostName = inetAddr.getHostName();
        } else {
            remoteHostName = "unknown";
        }
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }
    
    public String getRemoteHostName() {
        return remoteHostName;
    }
    
    public boolean isProxyUsed() {
        return proxy;
    }
    
    public String getForwardedForIPs() {
        return forwarded;
    }

}
